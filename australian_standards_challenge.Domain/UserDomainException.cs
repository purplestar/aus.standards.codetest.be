﻿using System;
using System.Collections.Generic;
using System.Text;

namespace australian_standards_challenge.Domain
{
    public class UserDomainException : Exception
    {
        public UserDomainException()
        { }

        public UserDomainException(string message)
            : base(message)
        { }

        public UserDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
