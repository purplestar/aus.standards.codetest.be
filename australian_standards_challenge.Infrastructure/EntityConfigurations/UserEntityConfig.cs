﻿using australian_standards_challenge.Domain;
using Car.Booking.Infrastructure.EntityConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace australian_standards_challenge.Infrastructure.EntityConfigurations
{
    class UserEntityConfig : BaseEntityConfig<User>
    {
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            base.Configure(builder);

            builder.ToTable("Users", UserContext.DEFAULT_SCHEMA);

            builder.Property(o => o.Id)
            .UseHiLo("userseq", UserContext.DEFAULT_SCHEMA);

            builder.Property<string>(x => x.FirstName).HasMaxLength(50).IsRequired(false);
            builder.Property<string>(x => x.LastName).HasMaxLength(50).IsRequired(false);
            builder.Property<string>(x => x.Email).HasMaxLength(50).IsRequired(false);
            builder.Property<string>(x => x.Gender).HasMaxLength(50).IsRequired(false);
            builder.Property<bool?>(x => x.Status).IsRequired(false);
        }
    }
}
