﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace australian_standards_challenge.API.Filters
{
    public class ApiCustomAuthorizeAttribute : AuthorizeAttribute
    {
        public ApiCustomAuthorizeAttribute() : base()
        {
        }

        public ApiCustomAuthorizeAttribute(params string[] roles) : base()
        {
            Roles = string.Join(",", roles);
        }
    }
}
