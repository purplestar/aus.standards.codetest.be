﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace australian_standards_challenge.Domain.SeedWork
{
    public class EventLog
    {
        public EventLog() { }

        public EventLog(DomainEvent @event, Guid transactionId, double timesSent)
        {
            EventId = @event.Id;
            CreationTime = @event.CreationDate;
            EventTypeName = @event.GetType().FullName;
            Content = JsonConvert.SerializeObject(@event);
            State = EventStateEnum.Published;
            TimesSent = timesSent;
            TransactionId = transactionId.ToString();
        }
        public Guid EventId { get; private set; } 

        public string EventTypeName { get; private set; }

        [NotMapped]
        public string EventTypeShortName => EventTypeName.Split('.')?.Last();

        [NotMapped]
        public DomainEvent IntegrationEvent { get; private set; }

        public EventStateEnum State { get; set; }

        /// <summary>
        /// Unit in seconds
        /// </summary>
        public double TimesSent { get; set; }

        public DateTime CreationTime { get; private set; }

        public string Content { get; private set; }

        public string TransactionId { get; private set; }

        public EventLog DeserializeJsonContent(Type type)
        {
            IntegrationEvent = JsonConvert.DeserializeObject(Content, type) as DomainEvent;
            return this;
        }
    }
}
