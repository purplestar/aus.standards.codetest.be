﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using australian_standards_challenge.Domain.SeedWork;

namespace australian_standards_challenge.Domain.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<IEnumerable<User>> GetAllAsync();
    }
}
