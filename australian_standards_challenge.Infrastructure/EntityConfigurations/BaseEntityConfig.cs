﻿using australian_standards_challenge.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Car.Booking.Infrastructure.EntityConfigurations
{
    public class BaseEntityConfig<T> : IEntityTypeConfiguration<T> where T : Entity
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Ignore(b => b.DomainEvents);
            //builder.Property<Guid>(x => x.IdentityGuid).IsRequired();
            builder.Property<bool>(x => x.IsDeleted).IsRequired();

            //Shadow properties
            builder.Property<DateTime>("CreatedDate").IsRequired().HasDefaultValueSql("getdate()");

            //Add index
            //builder.HasIndex(x => x.IdentityGuid).IsUnique(true);
            builder.HasIndex(x => x.IsDeleted);
        }
    }
}
