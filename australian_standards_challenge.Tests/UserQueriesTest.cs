using australian_standards_challenge.API.Application.Queries;
using australian_standards_challenge.Domain;
using australian_standards_challenge.Domain.Interfaces;
using australian_standards_challenge.Infrastructure;
using australian_standards_challenge.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;

namespace australian_standards_challenge.Tests
{
    public class UserQueriesTest
    {
        private readonly IUserRepository _userRepository;
        private static List<User> userList;

        public UserQueriesTest()
        {
            _userRepository = GetInMemoryUserRepository();
        }

        private IUserRepository GetInMemoryUserRepository()
        {
            DbContextOptions<UserContext> options;
            var builder = new DbContextOptionsBuilder<UserContext>();
            builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
            options = builder.Options;
            UserContext dataContext = new UserContext(options);
            dataContext.Database.EnsureDeleted();
            dataContext.Database.EnsureCreated();

            if (!dataContext.Users.Any())
            {
                string userListJson = File.ReadAllText("MOCK_DATA.json");
                userList = JsonConvert.DeserializeObject<List<User>>(userListJson);
                dataContext.Users.AddRange(userList);
                dataContext.SaveChanges();
            }

            return new UserRepository(dataContext);
        }

        [Fact]
        public async Task check_if_userList_match_total()
        {
            //Arrange
            var accountQueries = new UserQueries(_userRepository);

            //Act
            var list = await accountQueries.GetAllUsersAsync();

            //Assert
            Assert.Equal(1000, list.Count());
        }

        [Fact]
        public async Task check_if_userList_match_original_person_and_email()
        {
            //Arrange
            var accountQueries = new UserQueries(_userRepository);

            //Act
            var list = await accountQueries.GetAllUsersAsync();

            //Assert
            list.ToList().Select(x => $"{x.FirstName} {x.LastName}").Should().BeEquivalentTo(userList.ToList().Select(x => $"{x.FirstName} {x.LastName}"));
            list.ToList().Select(x => x.Email).Should().BeEquivalentTo(userList.ToList().Select(x => x.Email));
        }
    }
}
