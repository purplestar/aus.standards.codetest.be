﻿using australian_standards_challenge.API.Application.Models;
using australian_standards_challenge.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace australian_standards_challenge.API.Application.Queries
{
    public class UserQueries : IUserQueries
    {
        private readonly IUserRepository _userRepository;

        public UserQueries(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<IEnumerable<UserDTO>> GetAllUsersAsync()
        {
            var userList = await _userRepository.GetAllAsync();
            if (!userList.Any()) return null;

            else return UserDTO.ToUserDTOList(userList.ToList());
        }
    }
}
