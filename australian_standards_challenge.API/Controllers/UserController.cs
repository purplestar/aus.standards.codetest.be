﻿using australian_standards_challenge.API.Application.Queries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace australian_standards_challenge.API.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserQueries _userQueries;
        private readonly ILogger<UserController> _logger;

        public UserController(IUserQueries userQueries, ILogger<UserController> logger)
        {
            _userQueries = userQueries ?? throw new ArgumentNullException(nameof(userQueries));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [Route("list")]
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            _logger.LogInformation($"----- Sending query get userList at {DateTime.UtcNow}");
            return Ok(await _userQueries.GetAllUsersAsync());
        }
    }
}
