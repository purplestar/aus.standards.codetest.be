﻿using System;
using System.Collections.Generic;
using System.Text;

namespace australian_standards_challenge.Domain.SeedWork
{
    public enum EventStateEnum
    {
        NotPublished = 0,
        InProgress = 1,
        Published = 2,
        PublishedFailed = 3
    }
}
