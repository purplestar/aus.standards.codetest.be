﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace australian_standards_challenge.Domain.SeedWork
{
    public interface IUnitOfWork : IDisposable
    {
        List<INotification> GetAllDomainEvents();
        void RemoveAllDomainEvents();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<List<INotification>> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
