﻿using australian_standards_challenge.Domain;
using australian_standards_challenge.Domain.SeedWork;
using australian_standards_challenge.Infrastructure.EntityConfigurations;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace australian_standards_challenge.Infrastructure
{
    public class UserContext : DbContext, IUnitOfWork
    {
        public const string DEFAULT_SCHEMA = "Portal";

        /// <summary>
        /// The aggregate root table
        /// </summary>
        public DbSet<User> Users { get; set; }

        public DbSet<EventLog> EventLogs { get; set; }

        private IDbContextTransaction _currentTransaction;

        public IEnumerable<EntityEntry<Entity>> DomainEntities;

        public UserContext(DbContextOptions<UserContext> options) : base(options)
        {
            ChangeTracker.LazyLoadingEnabled = false;
            ChangeTracker.AutoDetectChangesEnabled = false;
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public IDbContextTransaction GetCurrentTransaction() => _currentTransaction;

        public bool HasActiveTransaction => _currentTransaction != null;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UserEntityConfig());
            modelBuilder.ApplyConfiguration(new EventLogEntityConfig());
        }

        public List<INotification> GetAllDomainEvents()
        {
            DomainEntities = this.ChangeTracker
               .Entries<Entity>()
               .Where(x => x.Entity.DomainEvents != null && x.Entity.DomainEvents.Any());

            var domainEvents = DomainEntities
                .SelectMany(x => x.Entity.DomainEvents)
                .ToList();

            RemoveAllDomainEvents();

            return domainEvents;
        }

        /// <summary>
        /// Remove all domain events in memory
        /// </summary>
        /// <param name="domainEntities"></param>
        public void RemoveAllDomainEvents()
        {
            DomainEntities.ToList()
                  .ForEach(entity => entity.Entity.ClearDomainEvents());
        }

        public async Task<List<INotification>> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            await base.SaveChangesAsync(cancellationToken);
            return GetAllDomainEvents();
        }

        public async Task<IDbContextTransaction> BeginTransactionAsync()
        {
            if (_currentTransaction != null) return null;

            _currentTransaction = await Database.BeginTransactionAsync(IsolationLevel.ReadCommitted);

            return _currentTransaction;
        }

        public async Task CommitTransactionAsync(IDbContextTransaction transaction)
        {
            if (transaction == null) throw new ArgumentNullException(nameof(transaction));
            if (transaction != _currentTransaction) throw new InvalidOperationException($"Transaction {transaction.TransactionId} is not current");

            try
            {
                await SaveChangesAsync();

                transaction.Commit();
            }
            catch
            {
                RollbackTransaction();
                throw;
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }

        public void RollbackTransaction()
        {
            try
            {
                _currentTransaction?.Rollback();
            }
            finally
            {
                if (_currentTransaction != null)
                {
                    _currentTransaction.Dispose();
                    _currentTransaction = null;
                }
            }
        }
    }
}
