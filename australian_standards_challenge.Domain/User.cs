﻿using australian_standards_challenge.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace australian_standards_challenge.Domain
{
    public class User : Entity, IAggregateRoot
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Email { get; private set; }
        public string Gender { get; private set; }
        public bool? Status { get; private set; }

        protected User()
        {
        }

        public User
            (
                string firstname,
                string lastname,
                string email,
                string gender,
                bool? status
            )
        {
            FirstName = firstname;
            LastName = lastname;
            Email = email;
            Gender = gender;
            Status = status;
        }
    }
}
