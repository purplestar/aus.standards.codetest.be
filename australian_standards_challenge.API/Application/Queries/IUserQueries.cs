﻿using australian_standards_challenge.API.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace australian_standards_challenge.API.Application.Queries
{
    public interface IUserQueries
    {
        Task<IEnumerable<UserDTO>> GetAllUsersAsync();
    }
}
