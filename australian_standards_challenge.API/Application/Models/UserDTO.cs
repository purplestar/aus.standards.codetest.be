﻿using australian_standards_challenge.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace australian_standards_challenge.API.Application.Models
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public bool? Status { get; set; }

        /// <summary>
        /// Make it reusable and maintainable, instead of dealing with hard maintenable automapping causing performance issue
        /// </summary>
        /// <param name="userList"></param>
        /// <returns></returns>
        public static List<UserDTO> ToUserDTOList(List<User> userList)
        {
            return userList.Select(x => new UserDTO()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email,
                Gender = x.Gender,
                Status = x.Status
            }).ToList();
        }
    }
}
